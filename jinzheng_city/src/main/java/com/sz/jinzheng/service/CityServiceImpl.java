package com.sz.jinzheng.service;

import com.sz.jinzheng.mapper.CityMapper;
import com.sz.jinzheng.pojo.City;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CityServiceImpl implements ICityService {

    @Autowired
    private CityMapper cityMapper;

    @Override
    public List<City> getAllCity() {
        return cityMapper.selectAll();
    }
}
