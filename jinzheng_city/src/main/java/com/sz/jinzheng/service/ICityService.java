package com.sz.jinzheng.service;

import com.sz.jinzheng.pojo.City;

import java.util.List;

public interface ICityService {

    List<City> getAllCity();
}
