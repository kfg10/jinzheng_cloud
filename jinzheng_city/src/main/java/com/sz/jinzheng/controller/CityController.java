package com.sz.jinzheng.controller;


import com.sz.jinzheng.pojo.City;
import com.sz.jinzheng.pojo.ResultBean;
import com.sz.jinzheng.service.ICityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/city")
public class CityController {

    @Autowired
    private ICityService iCityService;


    @RequestMapping("/list")
    public ResultBean list(){
        List<City> list = iCityService.getAllCity();
        return new ResultBean(0,"查询成功!",list);

    }
}
