package com.sz.jinzheng.mapper;

import com.sz.jinzheng.pojo.City;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.common.Mapper;

@Component
public interface CityMapper extends Mapper<City> {
}
