package com.sz.jinzheng.service;

import com.sz.jinzheng.pojo.User;

import java.util.List;

public interface IUserService {

    List<User> getAllUser();
}
