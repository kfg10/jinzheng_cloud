package com.sz.jinzheng.cityclients;

import com.sz.jinzheng.pojo.ResultBean;
import org.springframework.stereotype.Component;

@Component
public class CityClientsImpl implements CityClients {

    @Override
    public ResultBean getAllCitys() {
        System.out.println("熔断器起作用了...");
        return new ResultBean(444,"服务停止了。。", null);
    }
}
