package com.sz.jinzheng.cityclients;

import com.sz.jinzheng.pojo.ResultBean;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;

@Component
@FeignClient(value = "jinzheng-city", fallback = CityClientsImpl.class)
//如果血崩，则自己熔断
public interface CityClients {

    @RequestMapping("/city/list")
    public ResultBean getAllCitys();
}
