package com.sz.jinzheng;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@EnableEurekaClient   //Eureka客户端
@MapperScan("com.sz.jinzheng.mapper")
@EnableFeignClients    //微服务调用
public class UserStarter {


    public static void main(String[] args) {

        SpringApplication.run(UserStarter.class);
    }
}
