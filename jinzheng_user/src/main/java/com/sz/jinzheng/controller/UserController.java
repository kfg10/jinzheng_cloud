package com.sz.jinzheng.controller;

import com.sz.jinzheng.cityclients.CityClients;
import com.sz.jinzheng.pojo.ResultBean;
import com.sz.jinzheng.pojo.User;
import com.sz.jinzheng.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private IUserService iUserService;

    @Autowired
    private CityClients cityClients;

    @RequestMapping("/list")
    public ResultBean list(){
        List<User> list = iUserService.getAllUser();
        return new ResultBean(0,"查询成功!",list);
    }

    //调用城市微服务模块的查询所有的城市的功能：
    @RequestMapping("/city/list")
    public ResultBean getAllCitys(){
        return cityClients.getAllCitys();
    }
}
