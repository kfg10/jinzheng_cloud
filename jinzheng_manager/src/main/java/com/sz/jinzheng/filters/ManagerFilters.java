package com.sz.jinzheng.filters;

import com.netflix.discovery.util.StringUtil;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import com.sun.org.apache.regexp.internal.RE;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class ManagerFilters extends ZuulFilter {

    /***
     * //说明： 启动 jinzheng-manager 会发现过滤器已经执行 filterType：返回一个字符串代表过滤器的类型，
     * 在 zuul 中定义了四种不同生命周期的过 滤器类 型，具体如下：
     * pre ：可以在请求被路由之前调用
     * route ：在路由请求时候被调用
     * post ：在 route 和 error 过滤器之后被调用
     * error ：处理请求时发生错误时被调用
     * filterOrder：通过 int 值来定义过滤器的执行顺序
     * shouldFilter：返回一个 boolean 类型来判断该过滤器是否要执行，所以通过此函数可实现过滤器 的开关。在上例中，我们直接返回 true， 所以该过滤器总是生效
     * run：过滤器的具体逻辑
     *
     */

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        //run：过滤器的具体逻辑
        //1.得到当前上下文对象
        RequestContext currentContext = RequestContext.getCurrentContext();
        //2.得到请求对象
        HttpServletRequest request = currentContext.getRequest();
        //3.对特定的资源放行
        if(request.getMethod().equals("OPTIONS")) return null;
        if (request.getRequestURL().toString().contains("login")) return null;
        //4.得到请求头 auth
        String header = request.getHeader("auth");
        //5.判断是否为 null?如果不为空就放行！否则进行拦截！！！
        if (StringUtils.isNotBlank(header)){
            //org.apache.commons..lang包下的StringUtils  isNotBlank非空
            //6.放行，其实还需要进一步验证并解析token,此处省略了！！！jwt验证省略；
            return null;
        }else{
            //7.否则 就拦截并提示没有权限访问！！！
            currentContext.setSendZuulResponse(false);   //网关响应为false
            //阻止向下执行
            currentContext.setResponseStatusCode(403);    //响应状态码
            currentContext.setResponseBody("无权访问!");
            currentContext.getResponse().setContentType("text/html;charset=UTF-8");
            return null;
        }
    }
}
