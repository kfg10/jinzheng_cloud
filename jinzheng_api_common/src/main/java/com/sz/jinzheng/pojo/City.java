package com.sz.jinzheng.pojo;

import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "city")
public class City {

    @Id
    @KeySql(useGeneratedKeys = true)
    private Integer pid;
    private String pname;
    private Integer fid;

    public City() {
    }

    public City(Integer pid, String pname, Integer fid) {
        this.pid = pid;
        this.pname = pname;
        this.fid = fid;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public Integer getFid() {
        return fid;
    }

    public void setFid(Integer fid) {
        this.fid = fid;
    }

    @Override
    public String toString() {
        return "City{" +
                "pid=" + pid +
                ", pname='" + pname + '\'' +
                ", fid=" + fid +
                '}';
    }
}
